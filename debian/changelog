r-bioc-hdf5array (1.34.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 12 Jan 2025 21:46:12 +0100

r-bioc-hdf5array (1.34.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 01 Nov 2024 08:46:18 +0100

r-bioc-hdf5array (1.32.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * d/control: move unmanaged build-dep, architecture-is-64-bit, to the
    end of the list so that dh-update-R doesn't ignore the r-* deps.
  * Removed the patch that was previously cherry-picked from upstream,
    as it is now natively included.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 16 Aug 2024 14:14:34 +0200

r-bioc-hdf5array (1.32.0-1~0exp2) experimental; urgency=medium

  * Team upload.
  * Cherry-pick patch from upstream to fix errors not being handled correctly.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 12 Aug 2024 09:55:11 +0200

r-bioc-hdf5array (1.32.0-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 23 Jul 2024 11:48:45 +0200

r-bioc-hdf5array (1.32.0-1~0exp) experimental; urgency=medium

  * Team upload.
  * New upstream version. Closes: #1071342
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 09 Jul 2024 20:52:04 +0200

r-bioc-hdf5array (1.30.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  Set upstream metadata fields: Archive.

 -- Andreas Tille <tille@debian.org>  Tue, 20 Feb 2024 14:26:16 +0100

r-bioc-hdf5array (1.30.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 30 Nov 2023 13:01:23 +0100

r-bioc-hdf5array (1.28.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 26 Jul 2023 18:18:58 +0200

r-bioc-hdf5array (1.26.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 07:28:02 +0100

r-bioc-hdf5array (1.24.2-1) unstable; urgency=medium

  * Team upload
  * New upload version
  * Disable reprotest

 -- Andreas Tille <tille@debian.org>  Sun, 07 Aug 2022 13:08:30 +0200

r-bioc-hdf5array (1.24.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 07 Jun 2022 11:23:51 +0200

r-bioc-hdf5array (1.24.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 14 May 2022 22:37:13 +0200

r-bioc-hdf5array (1.22.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2021 08:13:29 +0100

r-bioc-hdf5array (1.20.0-2) unstable; urgency=medium

  * Team Upload.
  * d/rules: Remove test_H5ADMatrixSeed-class.R until zellkonverter
    gets accepted, revert once done

 -- Nilesh Patra <nilesh@debian.org>  Sun, 19 Sep 2021 03:05:49 +0530

r-bioc-hdf5array (1.20.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Refresh patches
  * Add B-D on r-bioc-rhdf5filters

 -- Nilesh Patra <nilesh@debian.org>  Sat, 11 Sep 2021 12:49:47 +0530

r-bioc-hdf5array (1.18.1-3) unstable; urgency=high

  * Team upload.
  * debian/docs: unneeded, removed

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 08 Feb 2021 09:59:14 +0100

r-bioc-hdf5array (1.18.1-2) unstable; urgency=medium

  * Team upload.
  * Fix pkg-r-autopkgtest, add missing dependency on r-cran-runit.
  * Remove debian/tests/run-unit-tests, as that duplicates that now-working
    pkg-r-autopkgtest

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 07 Feb 2021 13:19:29 +0100

r-bioc-hdf5array (1.18.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Sat, 06 Feb 2021 14:44:46 +0100

r-bioc-hdf5array (1.18.0-2) unstable; urgency=medium

  * Team upload.
  * debian/control:Turn off pkg-r-autopkgtest, the hand written autopkgtests
    do the same thing but work

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 22 Nov 2020 13:21:46 +0100

r-bioc-hdf5array (1.18.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 17 Nov 2020 12:25:09 +0100

r-bioc-hdf5array (1.16.1-2) unstable; urgency=medium

  * Team upload.

 -- Andreas Tille <tille@debian.org>  Thu, 17 Sep 2020 13:48:41 +0200

r-bioc-hdf5array (1.16.1-1) unstable; urgency=medium

  * Initial release (closes: #966553)

 -- Steffen Moeller <moeller@debian.org>  Sun, 06 Sep 2020 18:58:08 +0200
