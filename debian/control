Source: r-bioc-hdf5array
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Charles Plessy <plessy@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-sparsearray (>= 1.5.42),
               r-bioc-delayedarray (>= 0.31.8),
               r-bioc-rhdf5,
               r-cran-matrix,
               r-bioc-rhdf5filters,
               r-bioc-biocgenerics (>= 0.51.2),
               r-bioc-s4vectors,
               r-bioc-iranges,
               r-bioc-s4arrays,
               r-bioc-rhdf5lib,
               libhdf5-dev,
               architecture-is-64-bit
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-hdf5array
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-hdf5array.git
Homepage: https://bioconductor.org/packages/HDF5Array/
Rules-Requires-Root: no

Package: r-bioc-hdf5array
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: HDF5 backend for DelayedArray objects
 Implements the HDF5Array and TENxMatrix classes, 2 convenient
 and memory-efficient array-like containers for on-disk representation
 of HDF5 datasets. HDF5Array is for datasets that use the conventional
 (i.e. dense) HDF5 representation. TENxMatrix is for datasets that
 use the HDF5-based sparse matrix representation from 10x Genomics
 (e.g. the 1.3 Million Brain Cell Dataset). Both containers being
 DelayedArray extensions, they support all operations supported by
 DelayedArray objects. These operations can be either delayed or
 block-processed.
